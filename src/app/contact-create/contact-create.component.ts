import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit {

  contactName:  string  =  "";
  contactAddress:  string  =  "";
  contactSource:  string  =  "direct";
  contactGender:  string  =  "male";
  isDeleted  :  boolean  =  false;
  //date  =  new  FormControl(new  Date());

  constructor() { }

  ngOnInit() {
  }

  public  saveCustomer(){
      /* Typically this method will be used to send the contact form to a server to save it*/
  }


}
